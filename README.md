# README #

## fit_3d_circle_panel ##

### Description ###

Calculate the 3d circle parameters fit through three picked 3d points and, optionally, create a centre point and approximated 3d circle.

By 3d circle, we mean a circle that has been rotated in 3d space such that it may no longer appear circular when viewed in plan, but will be circular within its own plane.  This is in contrast to a circle with 3d levels currently able to be drawn in 12d Model using existing tools.

### Explanation ###

The mathematics are fairly straight-forward and hopefully the code is easy enough to follow.

#### Calculate parameters ####
- 3 non-collinear points in 3d space define a plane and also a triangle.  
- There also exists a unique circumscribed circle for these 3 points
- The parameters for the 3d circle- the centre coordinate and radius- are calculated via the barycentric coordinates method for circles.
- We use vectors to simplify the calculations.
- Wikipedia has a good explanation of the formulae- https://en.wikipedia.org/wiki/Circumscribed_circle#Cartesian_coordinates_from_cross-_and_dot-products

#### Create 3d circle ####
- Once we have the circle parameters, we can easily construct an approximated 3d circle
- This is an approximation, since we use straight-line segments between vertices
- We calculate the sweep angle to iterate based on the chord-to-arc tolerance
- We start at the 1st picked point
- We construct a rotation matrix based on the normal vector (i.e. the axis of rotation)
- Then we simply iterate the point, rotating it by the sweep aangle each time and storing the resulting coordinates
- We also need to translate these from the origin (0,0,0) to the circle centre
- Then we just create a super string from these coordinates

#### Notes ####
You could expand out and do these same calculations using basic trigonometry, however, using vectors and matrices is a lot easier.  There are also multiple ways of achieving the same thing- this code simply adopted the one I thought was easiest.


### Content ###

- **bin** - Contains a compiled version of the macro
- **src** - Contains the source code for the macro

### Compilation ###

#### Source Files ####
There are two source files included in this project:

1. **fit_3d_circle_panel.4dm** - the main source file. This is the one that should be compiled
2. **fit_3d_circle_panel-helpers.h** - contains some helper functions for the main application

And two additional files that you will be required to provide:

3. **set_ups.h** - shipped with 12d Model in the Program Files\Set_ups folder
4. **standard_library.h** - available for download from here- http://forums.12dmodel.com/downloads/Macros/includes.zip

#### Instructions ####
1. Place all the files in the same directory.
2. Compile from within 12d Model via the Utilities->Macros->Compile menu by browsing for and selecting the fit_3d_circle_panel.4dm file
3. If you have compilation errors, they should be written to a compile_log.4dl file and displayed in your default text editor.
4. If compilation is successful, a compiled macro file, fit_3d_circle_panel.4do, will be created.

Pre-compiled versions for 12d Model V14 are also available in the **bin** folder.

### Running ###

The compiled macro (*.4do) can be run by drag'n'drop into an open 12d Model session, from the menu Utilities->Macros->Run->Run or via customised toolbars, menus, chains, screen layout files and much more.  Refer to the 12d Model help for details on customisation.

### Other ###

This is an application (aka macro) written specifically for 12d Model software using the 12d Programming Language (12dPL).

The application requires 12d Model to run and can only be run from within a 12d Model session.

The application was written targeting 12d Model V14C2h or newer, but should be able to compiled for V12 (at least) and perhaps earlier versions.
