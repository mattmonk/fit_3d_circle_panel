/*****************************************
 *
 *	FILE:		fit_3d_circle_panel.4dm
 *
 *	DESCRIPTION:
 *  Calculates the parameters for a 3d circle
 *  fit through 3 picked points and optionally
 *  also creates a segmented 3d string approximating
 *  the 3d circle.
 *
 *
 *	AUTHOR:		Matthew Monk
 *	STARTED:	16/05/2021
 *
 *	REVISION HISTORY:
 *	1		16/05/2021	Initial macro
 *
 */


#define DEBUG								0

#define ATT_NAME_CENTRE						"centre"
#define ATT_NAME_RADIUS						"radius"
#define ATT_NAME_NORMAL						"normal"


// Prerequisites
// Standard 12d libraries
#include "set_ups.h"
#include "standard_library.h"
//
#include "fit_3d_circle_panel-helpers.h"

// #### GLOBAL VARIABLES ####
{
	// Appears in the panel title bar and about information printed to Output Window
	Text PROGRAM_NAME = "Fit 3d Circle";
	Text PROGRAM_AUTHOR = "Matthew Monk";
	Text PROGRAM_VERSION = "1.0";
	Text PROGRAM_DATE = "2021-05-17";
	Dynamic_Text Program_Info;
}

// ### Function Prototypes ###
Integer Manage_panel();
void Print_usage();

Integer Calculate_3d_circle_parameters(Vector3 p1, Vector3 p2, Vector3 p3,
										Vector3 &centre, Vector3 &normal, Real &radius);
Integer Create_point(Vector3 v, Element &elt);
Integer Create_circle_3d(Vector3 p1, Vector3 p2, Vector3 p3, Vector3 centre, Real radius, Vector3 normal,
							Real chord_arc_tol, Element &circle_elt);
Integer Validate_points(Vector3 p1, Vector3 p2, Vector3 p3, Colour_Message_Box &msg);
// ========== START ROUTINES ============


/*! @ingroup macro
 *	@brief
 *	Main entry point for macro.
 */
void main()
{
	Integer rv = -1;
	#if DEBUG==1
		Clear_console();
	#endif

	Print();
	Print(Pad_both(" " + PROGRAM_NAME + " v." + PROGRAM_VERSION + " ", "#", 50) + "\n");
	Print();

	///#### LAUNCH PANEL ####
	rv = Manage_panel();

	return;
}

/*! @ingroup macro
 *	@brief
 *	Displays a GUI panel and handles user interaction with panel.
 */
Integer Manage_panel()
{
	Integer rv = -1;

	#if VERSION_4D >= 1400
	Panel panel = Create_panel(PROGRAM_NAME, TRUE);
	#else
	Panel panel = Create_panel(PROGRAM_NAME);
	#endif
	// ### V & H GROUPS ###
	Vertical_Group vg_panel = Create_vertical_group(BALANCE_WIDGETS_OVER_HEIGHT);
	Vertical_Group vg_src = Create_vertical_group(ALL_WIDGETS_OWN_HEIGHT);
	Vertical_Group vg_main = Create_vertical_group(ALL_WIDGETS_OWN_HEIGHT);
	Vertical_Group vg_target = Create_vertical_group(ALL_WIDGETS_OWN_HEIGHT);

	Horizontal_Group hg_buttons = Create_button_group();

	Colour_Message_Box message = Create_colour_message_box(" ");

	// ### SOURCE BOX ###
	New_XYZ_Box src_pt1_box = Create_new_xyz_box("1st point", message);
	New_XYZ_Box src_pt2_box = Create_new_xyz_box("2nd point", message);
	New_XYZ_Box src_pt3_box = Create_new_xyz_box("3rd point", message);

	Append(src_pt1_box, vg_src);
	Append(src_pt2_box, vg_src);
	Append(src_pt3_box, vg_src);

	// ### MAIN ###
	Set_border(vg_main, "Calculated values");
	// Add extra widgets as necessary
	New_XYZ_Box centre_box = Create_new_xyz_box("Centre", message);
	Real_Box radius_box = Create_real_box("Radius", message);

	Append(centre_box, vg_main);
	Append(radius_box, vg_main);

	// ### TARGET BOX ###
	// Add output/target widgets, as necessary
	// Append to vg_target
	Named_Tick_Box create_centre_box = Create_named_tick_box("Create centre point", FALSE, "create_centre");
	Named_Tick_Box create_circle_box = Create_named_tick_box("Create circle element", TRUE, "create_circle");
	Real_Box chord_arc_box = Create_real_box("Chord-to-arc tolerance", message);
	Set_data(chord_arc_box, 0.1);

	Append(create_centre_box, vg_target);
	Append(create_circle_box, vg_target);
	Append(chord_arc_box, vg_target);

	// ### BUTTONS ###
	Button calc_button = Create_button("Calculate", "calculate");
	Button create_button = Create_button("Create", "create");
	Button finish_button = Create_finish_button("Finish", "finish");
	Button help_button = Create_help_button(panel, "Help");

	Append(calc_button, hg_buttons);
	Append(create_button, hg_buttons);
	Append(finish_button, hg_buttons);
	Append(help_button, hg_buttons);

	Append(vg_src, vg_panel);
	Append(vg_main, vg_panel);
	Append(vg_target, vg_panel);

	Append(message, vg_panel);
	Append(hg_buttons, vg_panel);

	Append(vg_panel, panel);

	Show_widget(panel);

	Integer doit = 1;

	while(doit)
	{
		Integer id = -1;
		Text    cmd = "";
		Text    msg = "";
		Integer ret = Wait_on_widgets(id,cmd,msg);  // this processes standard messages first ?

		rv = -1;

		if(cmd == "keystroke") continue;

		switch(id)
		{
			case Get_id(panel) :
			{
				if(cmd == "Panel Quit")
				{
					doit = 0;
				}
				if(cmd == "Panel About")
				{
					// Standard 12d About dialog
					about_panel(panel);
				}
			} break;		// End - Panel

			case Get_id(finish_button) :
			{
				if(cmd == "finish") doit = 0;
			} break;		// End - finish

			case Get_id(chord_arc_box) :
			{
				if (cmd == "real selected")
				{
					Real radius, chord_arc_tol;
					Null(radius);
					Null(chord_arc_tol);

					rv = Validate(chord_arc_box, chord_arc_tol);
					if (rv != TRUE)			break;
					if (xley(chord_arc_tol, 0.0))
					{
						Set_data(message, "Chord-to-arc tolerance cannot be zero or negative.", MESSAGE_LEVEL_ERROR);
						break;
					}
					else if (Is_null(chord_arc_tol))
					{
						Set_data(message, "Chord-to-arc tolerance cannot be null.", MESSAGE_LEVEL_ERROR);
						break;
					}

					rv = Validate(radius_box, radius);
					if (rv != TRUE)			break;
					if (Is_null(radius))	break;

				}
			} break;

			case Get_id(calc_button) :
			case Get_id(create_button) :
			{
				Real x1, y1, z1;
				Real x2, y2, z2;
				Real x3, y3, z3;
				Integer create_centre = FALSE;
				Integer create_circle = FALSE;
				Vector3 p1, p2, p3;

				Vector3 centre, normal;
				Real radius;
				Undo_List ul;
				Text status = "";

				Null(x1, y1, z1);
				Null(x2, y2, z2);
				Null(x3, y3, z3);
				Null(p1);
				Null(p2);
				Null(p3);
				Null(centre);
				Null(radius);

				// Clear/Reset calculated boxes
				Set_data(centre_box, "");
				Set_data(radius_box, "");
				Print();

				rv = Validate(src_pt1_box, x1, y1, z1);
				if (rv != TRUE)		break;
				if (Is_null(z1))	break;
				rv = Set_vector(p1, x1, y1, z1);
				if (rv != 0)		break;

				#if DEBUG==1
				Element pt1_elt;
				rv = Create_point(p1, pt1_elt);
				Set_name(pt1_elt, "PT-1");
				Set_model(pt1_elt, Get_model_create("DEBUG"));
				#endif // DEBUG

				rv = Validate(src_pt2_box, x2, y2, z2);
				if (rv != TRUE)		break;
				if (Is_null(z2))	break;
				rv = Set_vector(p2, x2, y2, z2);
				if (rv != 0)		break;

				#if DEBUG==1
				Element pt2_elt;
				rv = Create_point(p2, pt2_elt);
				Set_name(pt2_elt, "PT-2");
				Set_model(pt2_elt, Get_model_create("DEBUG"));
				#endif // DEBUG

				rv = Validate(src_pt3_box, x3, y3, z3);
				if (rv != TRUE)		break;
				if (Is_null(z3))	break;
				rv = Set_vector(p3, x3, y3, z3);
				if (rv != 0)		break;

				#if DEBUG==1
				Element pt3_elt;
				rv = Create_point(p3, pt3_elt);
				Set_name(pt3_elt, "PT-3");
				Set_model(pt3_elt, Get_model_create("DEBUG"));
				#endif // DEBUG

				// Validate points
				rv = Validate_points(p1, p2, p3, message);
				if (rv != 0)		break;

				//
				rv = Calculate_3d_circle_parameters(p1, p2, p3, centre, normal, radius);
				Print_debug_line(To_text(rv) + " : Calc params:");
				if (rv != 0)
				{
					status = "Problem calculating 3d circle parameters";
					Set_data(message, status, MESSAGE_LEVEL_ERROR);
					Print_log_line("ERROR : " + status, LOG_LINE_ERROR);
					break;
				}
				Real xc = Get_vector(centre, 1);
				Real yc = Get_vector(centre, 2);
				Real zc = Get_vector(centre, 3);

				Print_log_line("Calculated centre point: x = " + To_text(xc, 3) + " , y = " +
								To_text(yc, 3) + " , z = " + To_text(zc, 3), LOG_LINE_GENERAL, xc, yc, zc);
				Print_log_line("Calculated radius: R = " + To_text(radius, 3), LOG_LINE_GENERAL);
				Set_data(message, "3d circle parameters calculated", MESSAGE_LEVEL_GOOD);
				Set_data(centre_box, centre);
				Set_data(radius_box, radius);

				if (id == Get_id(create_button))
				{
					Real chord_arc_tol = 0.0;
					// Get ControlBar settings
					Text cb_name, cb_style;
					Integer cb_colour, cb_tinable;
					Model cb_model;
					Real cb_z, cb_weight;
					cb_name = "";
					cb_style = "1";
					cb_colour = cb_tinable = -1;
					Null(cb_model);
					Null(cb_z);
					Null(cb_weight);

					rv = Get_cad_controlbar(cb_name, cb_model, cb_colour, cb_z,cb_style, cb_weight, cb_tinable);
					if (rv != 0)
					{
						Set_data(message, "Cannot get Cad ControlBar settings", MESSAGE_LEVEL_ERROR);
						break;
					}
					if (Is_null(cb_weight))
					{
						Print("ControlBar Weight is null = " + To_text(cb_weight, 3));
						cb_weight = 0.0;
					}

					rv = Validate(create_centre_box, create_centre);
					if (create_centre)
					{
						Element centre_elt;
						Null(centre_elt);
						rv = Create_point(centre, centre_elt);
						if (rv != 0)
						{
							status = "Problem creating centre point element";
							Print_log_line("ERROR : " + status, LOG_LINE_ERROR, centre_elt, xc, yc, zc);
							Set_data(message, status, MESSAGE_LEVEL_ERROR);
							Element_delete(centre_elt);
							break;
						}
						// Properties
						Set_name(centre_elt, cb_name);
						Set_model(centre_elt, cb_model);
						Set_colour(centre_elt, cb_colour);
						Set_style(centre_elt, cb_style);
						Set_weight(centre_elt, cb_weight);
						Calc_extent(centre_elt);
						Append(Add_undo_add("create centre", centre_elt), ul);
						status = "Centre point element created";
						Print_log_line("INFO : " + status, LOG_LINE_GENERAL, centre_elt, xc, yc, zc);
						Set_data(message, status, MESSAGE_LEVEL_GOOD);
					}
					rv = Validate(create_circle_box, create_circle);
					rv = Validate(chord_arc_box, chord_arc_tol);
					if (rv != TRUE)		break;
					if (xgey(chord_arc_tol, radius))
					{
						Set_data(message, "Chord-to-arc tolerance must be less than the radius.", MESSAGE_LEVEL_ERROR);
						break;
					}
					else if (xley(chord_arc_tol, 0.0))
					{
						Set_data(message, "Chord-to-arc tolerance cannot be zero or negative.", MESSAGE_LEVEL_ERROR);
						break;
					}
					else if (Is_null(chord_arc_tol))
					{
						Set_data(message, "Chord-to-arc tolerance cannot be null.", MESSAGE_LEVEL_ERROR);
						break;
					}

					if (create_circle)
					{
						Element circle_elt;
						Null(circle_elt);
						rv = Create_circle_3d(p1, p2, p3, centre, radius, normal, chord_arc_tol, circle_elt);
						if (rv != 0)
						{
							status = "Problem creating 3d circle element";
							Print_log_line("ERROR : " + status + " (err: " + To_text(rv) + ")",
											LOG_LINE_ERROR, circle_elt);
							Set_data(message, status, MESSAGE_LEVEL_ERROR);
							Element_delete(circle_elt);
							break;
						}
						else if (!Element_exists(circle_elt))
						{
							status = "Created 3d circle element does not exist";
							Print_log_line("ERROR : " + status, LOG_LINE_ERROR, circle_elt);
							Set_data(message, status, MESSAGE_LEVEL_ERROR);
							Element_delete(circle_elt);
							break;
						}
						Set_name(circle_elt, cb_name);
						Set_model(circle_elt, cb_model);
						Set_colour(circle_elt, cb_colour);
						Set_style(circle_elt, cb_style);
						Set_weight(circle_elt, cb_weight);
						Calc_extent(circle_elt);
						Append(Add_undo_add("create circle", circle_elt), ul);
						status = "3d Circle element created";
						Print_log_line("INFO : " + status, LOG_LINE_GENERAL, circle_elt);
						Set_data(message, status, MESSAGE_LEVEL_GOOD);
					}
					Null(cb_model);
					Add_undo_list(PROGRAM_NAME, ul);
				}
				if (rv == 0)
				{
					Set_data(message, "Finished.", MESSAGE_LEVEL_GOOD);
					Integer move_cursor = (Getenv("MOVE_CURSOR_TO_FINISH_BUTTON_4D") == "1") ? TRUE : FALSE;
					Set_finish_button(finish_button, move_cursor);
				}
				else
				{

				}
			} break;		// End - process
		}
	}
	return 0;
}

Integer Validate_points(Vector3 p1, Vector3 p2, Vector3 p3, Colour_Message_Box &msg)
{
	Vector3 a = p2 - p1;
	Vector3 b = p3 - p1;
	Vector3 c = p3 - p2;

	Text status = "";

	if (xeqy(Get_vector_length(a), 0.0))
	{
		status = "Point 1 equal to Point 2";
		Set_data(msg, status, MESSAGE_LEVEL_ERROR);
		Print_log_line("ERROR : " + status, LOG_LINE_ERROR);
		return 1;
	}
	if (xeqy(Get_vector_length(b), 0.0))
	{
		status = "Point 1 equal to Point 3";
		Set_data(msg, status, MESSAGE_LEVEL_ERROR);
		Print_log_line("ERROR : " + status, LOG_LINE_ERROR);
		return 2;
	}
	if (xeqy(Get_vector_length(c), 0.0))
	{
		status = "Point 2 equal to Point 3";
		Set_data(msg, status, MESSAGE_LEVEL_ERROR);
		Print_log_line("ERROR : " + status, LOG_LINE_ERROR);
		return 3;
	}
	// Collinear test
	Vector3 cross = a ^ b;
	Real dbl_area = Get_vector_length(cross);

 	if (xeqy(dbl_area, 0.0))
	{
		status = "Points are collinear";
		Set_data(msg, status, MESSAGE_LEVEL_ERROR);
		Print_log_line("ERROR : " + status, LOG_LINE_ERROR);
		return 4;
	}

	return 0;
}

Integer Calculate_3d_circle_parameters(Vector3 p1, Vector3 p2, Vector3 p3,
										Vector3 &centre, Vector3 &normal, Real &radius)
{
	Integer rv = -1;
	Null(centre);
	Null(normal);
	// Sides of the triangle
	Vector3 a = p2 - p1;
	Vector3 b = p3 - p1;
	Vector3 c = p3 - p2;
	// Checks
	if (xeqy(Get_vector_length(a), 0.0))
	{
		return 1;
	}
	else if (xeqy(Get_vector_length(b), 0.0))
	{
		return 2;
	}
	else if (xeqy(Get_vector_length(c), 0.0))
	{
		return 3;
	}
	Vector3 n = a ^ b;
	Real nsql = Get_vector_length_squared(n);
	if (xeqy(nsql, 0.0))
	{
		return 4;		// Area is effectively zero
	}
	Real insql2 = 1.0 / (2.0 * nsql);
	Real aa = a * a;
	Real bb = b * b;

	centre = p1 + (b * (a*a) * (b*c) - a* (b*b) *(a*c)) * insql2;
	radius = Sqrt((a*a) * (b*b) * (c*c) * insql2 * 0.5);
	normal = Get_vector_normalize(n);

	return 0;
}

Integer Create_point(Vector3 v, Element &elt)
{
	Integer rv = -1;
	Null(elt);

	if (Is_null(Get_vector(v, 1)))		return 1;
	if (Is_null(Get_vector(v, 2)))		return 2;

	Integer flag1 = String_Super_Bit(ZCoord_Value);
	Integer flag2 = 0;

	elt = Create_super(flag1, flag2, 1);
	if (!Element_exists(elt))	return 3;
	rv = Set_super_vertex_coord(elt, 1, Get_vector(v, 1), Get_vector(v, 2), Get_vector(v, 3));
	return rv;
}

Integer Create_circle_3d(Vector3 p1, Vector3 p2, Vector3 p3, Vector3 centre, Real radius, Vector3 normal,
							Real chord_arc_tol, Element &circle_elt)
{
	Integer rv = -1;
	Null(circle_elt);
	// Get centre points
	Real xc = Get_vector(centre, 1);
	Real yc = Get_vector(centre, 2);
	Real zc = Get_vector(centre, 3);
	// Get radial vectors from centre to points
	Vector3 r1 = p1 - centre;
	Vector3 r2 = p2 - centre;
	Vector3 r3 = p3 - centre;
	// We don't really have to normalize it
	Vector3 axis = Get_vector_normalize(normal);
	Print_debug_line("Axis = " + To_text(axis));
	// We'll take the 1st picked point as the base axis on the circle plane
	// Angles will be measured from this.
	Vector3 orth_axis = Get_vector_normalize(r1);

	// Calculate sweep angle for chord-to-arc tolerance
	Real angle;
	rv = Calculate_chord_arc_tolerance_angle(radius, chord_arc_tol, angle);
	Print_debug_line(rv, "Calc chord-arc angle = " + To_text(angle, 4));
	if (rv != 0)
	{
		angle = 2 * Pi() / 100;
	}
	Integer num_pts = 2 * Pi() / angle + 1;
	Print_debug_line("Num pts = " + To_text(num_pts));
	// Adjust sweep angle, so we're below the chord-to-arc tolerance
	// and also have regularly spaced points and no left-over bits.
	angle = 2 * Pi() / num_pts;
	Print_debug_line("adjusted angle = " + To_text(angle, 4));

	Real x[num_pts];
	Real y[num_pts];
	Real z[num_pts];
	Real r[num_pts];
	Integer f[num_pts];

	Vector3 pt = orth_axis * radius;		// Could also have used r1 diretly, but leave this in case we want to
											// align to world axes or something else, e.g. quadrants
	Matrix4 rotation = Get_rotation_matrix(axis, angle);

	Attributes str_atts;
	Set_attribute(str_atts, "point1", p1);
	Set_attribute(str_atts, "point2", p2);
	Set_attribute(str_atts, "point3", p3);
	Set_attribute(str_atts, "centre", centre);
	Set_attribute(str_atts, "radius", radius);
	Set_attribute(str_atts, "normal", normal);
	Set_attribute(str_atts, "chord_to_arc_tolerance", chord_arc_tol);

	Real swept_angle = 0.0;
	Integer i = 1;

	while ((i <= num_pts) && (swept_angle < Two_pi()))
	{
		x[i] = Get_vector(pt, 1) + xc;
		y[i] = Get_vector(pt, 2) + yc;
		z[i] = Get_vector(pt, 3) + zc;
		Null(r[i]);
		f[i] = 0;
		#if DEBUG==1
		Print_debug_line("Iteration = " + To_text(i));
		Print_log_line("Reg Pt " + To_text(i) + " : x = " + To_text(x[i], 3) + " , y = " + To_text(y[i], 3) +
						" , z = " + To_text(z[i], 3) + " , angle = " + To_text(swept_angle, 3), LOG_LINE_GENERAL,
						x[i], y[i], z[i]);

		#endif // DEBUG
		pt = rotation * pt;
		++i;
		swept_angle += angle;
	};

	Integer flag1 = String_Super_Bit(ZCoord_Array);
	Integer flag2 = 0;

	circle_elt = Create_super(flag1, flag2, x, y, z, r, f, num_pts);

	String_close(circle_elt);
	Set_attribute(circle_elt, PROGRAM_NAME, str_atts);

	return 0;
}


