#ifndef FIT_3D_CIRCLE_PANEL_HELPERS
#define FIT_3D_CIRCLE_PANEL_HELPERS

///#### PRE-REQUISITES ####
#include "set_ups.h"

///#### DECLARATION ####

// Log Line Levels - in case they're not already defined in something like set_ups.h
#ifndef LOG_LINE_NONE
#define LOG_LINE_NONE				0			/*!<	Not a log line, a normal output message */
#endif // LOG_LINE_NONE
#ifndef LOG_LINE_GENERAL
#define LOG_LINE_GENERAL			1			/*!<	A general log line with green exclamation mark */
#endif // LOG_LINE_GENERAL
#ifndef LOG_LINE_WARNING
#define LOG_LINE_WARNING			2			/*!<	A warning log line with orange exclamation mark */
#endif // LOG_LINE_WARNING
#ifndef LOG_LINE_ERROR
#define LOG_LINE_ERROR				3			/*!<	An error log line with red exclamation mark */
#endif // LOG_LINE_ERROR


void Null(Real &a, Real &b, Real &c);
Integer Null(Vector2 &a);

Integer Set_data(New_XYZ_Box &box, Vector3 v);
Text To_text(Vector3 v);

Integer Get_angle_signed(Vector3 v1, Vector3 v2, Real &angle);
Integer Calculate_chord_arc_tolerance_angle(Real radius, Real chord_arc_tol, Real &angle);

Integer Is_type(Element elt, Text type);
Integer Get_data(Element elt, Text &str_name, Uid &str_id, Text &model_name, Uid &model_id);

Log_Line Create_log_line(Text msg, Integer level);
Log_Line Create_log_line(Text msg, Integer level, Element elt);
Log_Line Create_log_line(Text msg, Integer level, Element elt, Real x, Real y, Real z);
Log_Line Create_log_line(Text msg, Integer level, Real x, Real y, Real z);

void Print_log_line(Text msg, Integer log_level);
void Print_log_line(Text msg, Integer log_level, Element elt);
void Print_log_line(Text msg, Integer log_level, Element elt, Real x, Real y, Real z);
void Print_log_line(Text msg, Integer log_level, Real x, Real y, Real z);

void Print_line(Text msg);
void Print_debug(Text msg);
void Print_debug_line(Text msg);
void Print_debug_line(Integer rv, Text msg);

Real Radians_to_degrees(Real radians);

Integer Set_attribute(Attributes &parent, Text name, Vector3 v);

///#### IMPLEMENTATION ####

void Null(Real &a, Real &b, Real &c)
{
	Null(a);
	Null(b);
	Null(c);
	return;
}

Integer Null(Vector3 &a)
{
	Real x;
	Null(x);
	Integer rv = Set_vector(a, x);
	return rv;
}

Integer Set_data(New_XYZ_Box &box, Vector3 v)
{
	Integer rv = -1;
	rv = Set_data(box, Get_vector(v, 1), Get_vector(v, 2), Get_vector(v, 3));
	return rv;
}

Text To_text(Vector3 v)
{
	Text result = "";
	result = " ( ";
	result += To_text(Get_vector(v, 1));
	result += " , ";
	result += To_text(Get_vector(v, 2));
	result += " , ";
	result += To_text(Get_vector(v, 3));
	result += " ) ";
	return result;
}

Integer Sign(Real value)
{
	if (value > 0)		return 1;
	if (value < 0)		return -1;
	return 0;
}

Integer Calculate_chord_arc_tolerance_angle(Real radius, Real chord_arc_tol, Real &angle)
{
	Integer rv = -1;
	Null(angle);

	if (Is_null(radius))				return 1;
	radius = Absolute(radius);
	if (xeqy(radius, 0.0))				return 2;
	if (Is_null(chord_arc_tol))			return 3;
	if (xgty(chord_arc_tol, radius))	return 4;
	chord_arc_tol = Absolute(chord_arc_tol);

	Real c = 1.0 - ( chord_arc_tol / radius );
	angle = 2 * Acos(c);
	return 0;
}

Integer Is_type(Element elt, Text type)
{
	if (Element_exists(elt))
	{
		Text elt_type = "";
		Get_type(elt, elt_type);
		if (elt_type == type)	return 1;
	}
	return 0;
}

Integer Get_data(Element elt, Text &str_name, Uid &str_id, Text &model_name, Uid &model_id)
{
	Integer rv = -1;
	str_name = model_name = "";
	Null(str_id); Null(model_id);

	if (!Element_exists(elt))		return 1;

	Model model;
	Null(model);

	rv = Get_name(elt, str_name);
	if (rv != 0)				return 2;
	rv = Get_id(elt, str_id);
	if (rv != 0)				return 3;
	// Tin family can exist on multiple models
	if ( (!Is_type(elt, "Tin")) &&
		 (!Is_type(elt, "SuperTin")) &&
		 (!Is_type(elt, "Grid_Tin")) )
	{
		rv = Get_model(elt, model);
		if (rv != 0)				return 4;
		rv = Get_name(model, model_name);
		if (rv != 0)				return 5;
		rv = Get_id(model, model_id);
		if (rv != 0)				return 6;
	}
	return 0;
}

Log_Line Create_log_line(Text msg, Integer level)
{
	return Create_text_log_line(msg, level);
}

Log_Line Create_log_line(Text msg, Integer level, Element elt)
{
	Integer rv = -1;

	Text str_name, model_name;
	Uid model_id, string_id;
	str_name = model_name = "";
	Null(model_id);
	Null(string_id);
	rv = Get_data(elt, str_name, string_id, model_name, model_id);

	return Create_highlight_string_log_line(msg, level, model_id, string_id);
}

Log_Line Create_log_line(Text msg, Integer level, Element elt, Real x, Real y, Real z)
{
	Integer rv = -1;

	Text str_name, model_name;
	Uid model_id, string_id;
	str_name = model_name = "";
	Null(model_id);
	Null(string_id);

	rv = Get_data(elt, str_name, string_id, model_name, model_id);

	return Create_highlight_string_log_line(msg, level, model_id, string_id, x, y, z);
}

Log_Line Create_log_line(Text msg, Integer level, Real x, Real y, Real z)
{
	return Create_highlight_point_log_line(msg, level, x, y, z);
}

void Print_log_line(Text msg, Integer log_level)
{
	Integer is_error = (log_level >= LOG_LINE_WARNING);
	#if VERSION_4D >= 1000
	Log_Line log_line = Create_log_line(msg, log_level);
	Print_log_line(log_line, is_error);
	#else
	Print(msg + "\n");
	#endif
    return;
}

void Print_log_line(Text msg, Integer log_level, Element elt)
{
	Integer is_error = (log_level >= LOG_LINE_WARNING);
	#if VERSION_4D >= 1000
	Log_Line log_line = Create_log_line(msg, log_level, elt);
	Print_log_line(log_line, is_error);
	#else
	Print(msg + "\n");
	#endif
    return;
}

void Print_log_line(Text msg, Integer log_level, Real x, Real y, Real z)
{
	Integer is_error = (log_level >= LOG_LINE_WARNING);
	#if VERSION_4D >= 1000
	Log_Line log_line = Create_log_line(msg, log_level, x, y, z);
	Print_log_line(log_line, is_error);
	#else
	Print(msg + "\n");
	#endif
    return;
}

void Print_log_line(Text msg, Integer log_level, Element elt, Real x, Real y, Real z)
{
	Integer is_error = (log_level >= LOG_LINE_WARNING);
	#if VERSION_4D >= 1000
	Log_Line log_line = Create_log_line(msg, log_level, elt, x, y, z);
	Print_log_line(log_line, is_error);
	#else
	Print(msg + "\n");
	#endif
    return;
}

void Print_line(Text msg)
{
	Print(msg);
	Print();
	return;
}

void Print_debug(Text msg)
{
	#if DEBUG
	Print(msg);
	#endif
	return;
}

void Print_debug_line(Text msg)
{
	#if DEBUG==1
	Print_line(msg);
	#endif // DEBUG
	return;
}

void Print_debug_line(Integer rv, Text msg)
{
	#if DEBUG==1
	Print_line(To_text(rv) + " : " + msg);
	#endif // DEBUG
	return;
}

Real Radians_to_degrees(Real radians)
{
	Integer rv = -1;
	Real degrees;
	Null(degrees);
	if (!Is_null(radians))
	{
		degrees = radians * 180.0 / Pi();
	}
	return degrees;
}

Text Pad_both(Text input, Text pad_char, Integer width)
{
	Text output = input;
	Integer side = 0;	// 0 = left, 1 = right

	if (Text_length(output) > width)	return output;

	while (Text_length(output) < width)
	{
		if (side == 0)
		{
			output = pad_char + output;
			side = 1;
		}
		else
		{
			output = output + pad_char;
			side = 0;
		}
	}
	return output;
}

Integer Set_attribute(Attributes &parent, Text name, Vector3 v)
{
	Integer rv = -1;

	rv = Set_attribute(parent, name + "/x", Get_vector(v, 1));
	rv = Set_attribute(parent, name + "/y", Get_vector(v, 2));
	rv = Set_attribute(parent, name + "/z", Get_vector(v, 3));

	return 0;
}

#endif // FIT_3D_CIRCLE_PANEL_HELPERS




